package practica01_04;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class Departament {
	@Value("${id}")
	private int id;
	@Value("${nom}")
	private String nom;
	@Value("${email}")
	private String email;
	@Autowired
	private Organitzacio organitzacio;
	private Personal capDeDepartament;
	private ArrayList<Personal> llistaPersonal;
	private informeDepartament informe;
	
	@Autowired
	public Departament(int id, String nom, String email, Organitzacio organitzacio, Personal capDeDepartament,
			ArrayList<Personal> llistaPersonal, informeDepartament informe) {
		this.id = id;
		this.nom = nom;
		this.email = email;
		this.organitzacio = organitzacio;
		this.capDeDepartament = capDeDepartament;
		this.llistaPersonal = llistaPersonal;
		this.informe = informe;
	}
	
	public Departament() {
		this.id = 9;
		this.nom = "nom2";
		this.email = "mail2@mail.com";
		this.organitzacio = new Organitzacio();
		this.capDeDepartament = new Personal();
		this.llistaPersonal = new ArrayList<Personal>();
		this.informe = new informeDepartament();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}
	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}
	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}
	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}
	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}
	public informeDepartament getInforme() {
		return informe;
	}
	public void setInforme(informeDepartament informe) {
		this.informe = informe;
	}
	
	
}
