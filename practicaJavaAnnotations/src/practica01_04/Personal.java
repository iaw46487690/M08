package practica01_04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("dep-A-per-1")

public class Personal {
	
	private String dni;
	private String nom;
	private String cognom;
	private String email;
	private int numDepartament;
	
	@Autowired
	public Personal(String dni, String nom, String cognom, String email, int numDepartament) {
		super();
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.email = email;
		this.numDepartament = numDepartament;
	}
	
	public Personal() {
		this.dni = "123456987v";
		this.nom = "Alejandro";
		this.cognom = "Carreño";
		this.email = "hola@gmail.com";
		this.numDepartament = 4;
	}
	
	@Override
	public String toString() {
		return "\nPersonal\nDni = " + dni + 
				"\nNom = " + nom + 
				"\nCognom = " + cognom + 
				"\nEmail = " + email + 
				"\nNumDepartament = "+ numDepartament + "\n";
	}
	
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getNumDepartament() {
		return numDepartament;
	}

	public void setNumDepartament(int numDepartament) {
		this.numDepartament = numDepartament;
	}
}
