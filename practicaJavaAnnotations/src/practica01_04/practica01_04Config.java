package practica01_04;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("practicaJavaAnnotations")
@PropertySource("classpath:practica01-04Departaments.properties")
public class practica01_04Config {

	@Bean
	public Departament departament() {
		return new Departament();
	}
	
	@Bean
	public informeDepartament informe() {
		return new informeDepartament();
	}
	
	@Bean
	public Organitzacio organitzacio() {
		return new Organitzacio();
	}
	
}