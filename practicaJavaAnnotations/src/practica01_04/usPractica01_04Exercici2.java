package practica01_04;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usPractica01_04Exercici2 {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Practica01-04.xml");

		Departament dept1 = contexte.getBean("departament", Departament.class);
	
		System.out.println("dept1.getId(): " + dept1.getId());
		System.out.println("dept1.getNom(): " + dept1.getNom());
		System.out.println("dept1.getEmail(): " + dept1.getEmail());
		System.out.println("dept1.getOrganitzacio(): " + dept1.getOrganitzacio().getNom());
		System.out.println("dept1.getCapDeDepartament(): " + dept1.getCapDeDepartament().getDni());
		System.out.println("dept1.getLlistaPersonal(): " + dept1.getLlistaPersonal());
		System.out.println("dept1.getInforme(): " + dept1.getInforme().getTextAImprimirPerPantalla());
		contexte.close();
	}

}
