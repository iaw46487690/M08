package practica01_04;

public class informeDepartament {
	private String textAImprimirPerPantalla;

	public informeDepartament(String textAImprimirPerPantalla) {
		this.textAImprimirPerPantalla = textAImprimirPerPantalla;
	}

	public informeDepartament() {
		this.textAImprimirPerPantalla = "Aquest es un informe";
	}

	public String getTextAImprimirPerPantalla() {
		return textAImprimirPerPantalla;
	}

	public void setTextAImprimirPerPantalla(String textAImprimirPerPantalla) {
		this.textAImprimirPerPantalla = textAImprimirPerPantalla;
	}
}
