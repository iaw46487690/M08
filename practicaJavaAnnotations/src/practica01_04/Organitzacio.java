package practica01_04;

import org.springframework.beans.factory.annotation.Autowired;

public class Organitzacio {
	private String nom;
	private String departament;
	@Autowired
	private informeDepartament informe;
	
	public Organitzacio(String nom, String departament, informeDepartament informe) {
		this.nom = nom;
		this.departament = departament;
		this.informe = new informeDepartament();
	}
	
	public Organitzacio() {
		this.nom = "AFK";
		this.departament = "dept6";
		this.informe = new informeDepartament("Informe 3");
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDepartament() {
		return departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

	public informeDepartament getInforme() {
		return informe;
	}

	public void setInforme(informeDepartament informe) {
		this.informe = informe;
	}
}
