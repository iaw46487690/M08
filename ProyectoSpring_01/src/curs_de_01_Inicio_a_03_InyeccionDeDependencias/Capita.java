package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Capita implements Tripulants {
	
	private InformeInterface informeNou;
	
	public Capita (InformeInterface inf) {
		this.informeNou = inf;
	}
	
	public String agafarTarees() {
		return "Aquestes són les tasques del capità";
	}
	
	@Override
	public String agafarInforme() {
		return "Informe del capità. " + informeNou.getInforme();
	}
}
