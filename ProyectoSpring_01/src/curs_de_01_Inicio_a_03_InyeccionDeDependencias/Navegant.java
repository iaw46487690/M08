package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Navegant implements Tripulants {
	
	private InformeInterface informeNou;
	
	public void setInformeNou(InformeInterface informeNou) {
		this.informeNou = informeNou;
	}
	
	@Override
	public String agafarTarees() {
		return "agafarTarees(): Sóc el navegant de la nau Leonov.";
	}

	@Override
	public String agafarInforme() {
		return "agafarInforme: Informe del navegant. " + informeNou.getInforme();
	}
	
	public void metodeInit() {
		System.out.println("Acabem de llançar el mètode init() de la classe Navegant.class just abans de que el bean estigui llest");
	}
	
	public void metodeDestroy() {
		System.out.println("Acabem de llançar el mètode destroy() de la classe Navegant.java just després de que el bean hagi estat destruit (hagi mort).");
	}
}
