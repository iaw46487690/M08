package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usSingletonPrototype {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_SingletonPrototype.xml");
		
		Capita capita_1 = contexte.getBean("tripulantCapita", Capita.class);
		Capita capita_2 = contexte.getBean("tripulantCapita", Capita.class);
		
		Electronic electronic_1 = contexte.getBean("tripulantElectronic", Electronic.class);
		Electronic electronic_2 = contexte.getBean("tripulantElectronic", Electronic.class);
		
		// CAPITA
		System.out.println("Patró SINGLETON:");
		System.out.println("Dir. de memòria de capita_1: " + capita_1);
		System.out.println("Dir. de memòria de capita_2: " + capita_2);
		
		if (capita_1 == capita_2) {
			System.out.println("capita_1 i capita_2 SI són el mateix objecte, per tant, apunten al mateix objecte.");
		} else {
			System.out.println("capita_1 i capita_2 NO són el mateix objecte, per tant, no apunten al mateix objecte.");
		}
		System.out.println();
		
		// ELECTRONIC
		System.out.println("Patró PROTOTYPE:");
		System.out.println("Dir. de memòria de electronic_1: " + electronic_1);
		System.out.println("Dir. de memòria de electronic_2: " + electronic_2);
		
		if (electronic_1 == electronic_2) {
			System.out.println("electronic_1 i electronic_2 SI són el mateix objecte, per tant, apunten al mateix objecte.");
		} else {
			System.out.println("electronic_1 i electronic_2 NO són el mateix objecte, per tant, no apunten al mateix objecte.");
		}
		System.out.println();
		
		contexte.close();
	}

}
