package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsTripulants {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Tripulants capita = contexte.getBean("tripulantCapita", Tripulants.class);
		System.out.println(capita.agafarTarees());
		System.out.println(capita.agafarInforme());
		System.out.println();
		
		Tripulants maquinista = contexte.getBean("tripulantMaquinista", Tripulants.class);
		System.out.println(maquinista.agafarTarees());
		System.out.println();
		
		Electronic maquinista_2 = contexte.getBean("tripulantElectronic", Electronic.class);
		System.out.println("Email: " + maquinista_2.getEmail());
		System.out.println("Departament: " + maquinista_2.getNomDepartament());
		System.out.println();
		
		Tripulants electronic = contexte.getBean("tripulantElectronic", Tripulants.class);
		System.out.println(electronic.agafarTarees());
		System.out.println(electronic.agafarInforme());
		System.out.println();
		
		Electronic electronic_2 = contexte.getBean("tripulantElectronic", Electronic.class);
		System.out.println("Email: " + electronic_2.getEmail());
		System.out.println("Departament: " + electronic_2.getNomDepartament());
		System.out.println();
		
		contexte.close();
	}

}
