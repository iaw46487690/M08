package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Electronic implements Tripulants {
	
	private InformeInterface InformeNou;
	
	public void setInformeNou(InformeInterface informeNou) {
		this.InformeNou = informeNou;
	}
	
	private String email;
	private String nomDepartament;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomDepartament() {
		return nomDepartament;
	}

	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}

	@Override
	public String agafarTarees() {
		return "Aquestes són les tasques del electronic";
	}
	
	@Override
	public String agafarInforme() {
		return "Informe del tecnic en electrònica. " + InformeNou.getInforme();
	}
}
