package exercici;

import java.time.LocalDate;
import java.util.ArrayList;

public class Ruta {
	
	private int id;
	private String nom;
	private Waypoint waypoint;
	private ArrayList<Waypoint> llistaWaypoints;
	private LocalDate dataCreacio;
	
	public Ruta (int id, String nom, Waypoint waypoint, ArrayList<Waypoint> llistaWaypoints, LocalDate dataCreacio) {
		this.id = id;
		this.nom = nom;
		this.waypoint = waypoint;
		this.llistaWaypoints = llistaWaypoints;
		this.dataCreacio = dataCreacio;
	}
	
	public Ruta () {
		this.id = 1;
		this.nom = "Terra - Mart";
		this.waypoint = new Waypoint();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Waypoint getWaypoint() {
		return waypoint;
	}

	public void setWaypoint(Waypoint waypoint) {
		this.waypoint = waypoint;
	}

	public ArrayList<Waypoint> getLlistaWaypoints() {
		return llistaWaypoints;
	}

	public void setLlistaWaypoints(ArrayList<Waypoint> llistaWaypoints) {
		this.llistaWaypoints = llistaWaypoints;
	}

	public LocalDate getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
}
