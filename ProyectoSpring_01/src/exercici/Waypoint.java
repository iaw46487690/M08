package exercici;

import java.time.LocalDate;

public class Waypoint {
	private int id;
	private String nom;
	private LocalDate dataCreacio;
	
	public Waypoint (int id, String nom, LocalDate dataCreacio) {
		this.id = id;
		this.nom = nom;
		this.dataCreacio = dataCreacio;
	}
	
	public Waypoint () {
		this.id = 200;
		this.nom = "Venus";
		this.dataCreacio = LocalDate.parse("2020-10-19");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public LocalDate getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
}
