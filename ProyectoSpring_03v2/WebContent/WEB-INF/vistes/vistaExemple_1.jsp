<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>vistaExemple_1.jsp</title>
</head>
<body>
	<!-- Aquí hem de ficar el nostre codi (el que construirà la pàgina web que farà d'interficie amb l'usuari) .-->
	
	<center>
		<h1>1r exemple de MVC amb Spring</h1>
		(hem cridat a vistaExemple_1.jsp)
	</center>
	<br>
	<br>
	<!-- Ara creem una crida al formulari que volem que ompli l'usuari (video 28) -->
	<!-- Com a direcció del formulari hem de posar el valor del paràmetre del @RequestMapping("/mostrarFormulari") que tenim en FormulariWebControlador.java -->
	<a href="mostrarFormulari">Anar al formulari</a> 

</body>
</html>
