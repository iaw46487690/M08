package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class EspecialistaEnECM implements Tripulants {
	
	@Value("${emailDepartamentECM}")
	private String email;
	
	@Value("${departamentECMNom}")
	private String departamentNom;
	
	private InformeElectronicaInterface informeEspecialistaEnECM;
	
	private InformeElectronicaInterface informeEspecialistaEnESM;
	private InformeElectronicaInterface informeEspecialistaEnEPM;
	
	@Autowired
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM,
			InformeElectronicaInterface informeEspecialistaEnESM,
			InformeElectronicaInterface informeEspecialistaEnEPM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
		this.informeEspecialistaEnESM = informeEspecialistaEnESM;
		this.informeEspecialistaEnEPM = informeEspecialistaEnEPM;
	}

	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}
	
	public String getEmail() {
		return email;
	}

	public String getDepartamentNom() {
		return departamentNom;
	}

	@Override
	public String agafarTarees() {
		return "EspecialistaEnECM: agafarTarees(): arreglar la electrònica d'Infermeria. ";
	}

	@Override
	public String agafarInformes() {
		return informeEspecialistaEnECM.getInformeElectronica();
	}
	
	public String imprimirTripleInforme() {
		String cadena = "EspecialistaEnECM: imprimirTripleInforme(): \n \t" +
				"Informe ECM: " + informeEspecialistaEnECM.getInformeElectronica() + "\n \t" +
				"Informe ESM: " + informeEspecialistaEnESM.getInformeElectronica() + "\n \t" +
				"Informe EPM: " + informeEspecialistaEnEPM.getInformeElectronica();
		return cadena;
	}
}
