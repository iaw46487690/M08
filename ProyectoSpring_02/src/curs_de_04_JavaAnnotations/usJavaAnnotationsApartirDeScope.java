package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotationsApartirDeScope {

	public static void main(String[] args) {
		System.out.println("---------- usJavaAnnotationsApartirDeScope - INICI ----------");
		System.out.println();
		System.out.println("ABANS DE CARREGAR EL CONTEXT.");
		System.out.println();
		
		//ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		Tripulants especialistaEnECM_1 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		System.out.println("Tripulants especialistaEnECM.agafarInforme(): " + especialistaEnECM_1.agafarInformes());
		System.out.println("Tripulants especialistaEnECM.agafarTarees(): " + especialistaEnECM_1.agafarTarees());
		
		EspecialistaEnECM especialistaEnECM_2 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		System.out.println("EspecialistaEnECM especialistaEnECM.agafarTarees(): " + especialistaEnECM_2.agafarTarees());
		System.out.println("EspecialistaEnECM especialistaEnECM.getEmail(): " + especialistaEnECM_2.getEmail());
		System.out.println("EspecialistaEnECM especialistaEnECM.getDepartamentNom(): " + especialistaEnECM_2.getDepartamentNom());
		
		System.out.println();
		System.out.println();
		Tripulants doctor_1 = contexte.getBean("doctorNau", Doctor.class);
		Tripulants doctor_2 = contexte.getBean("doctorNau", Doctor.class);
		
		System.out.println("Patró SINGLETON:");
		System.out.println("Dir. de memòria de doctor_1: " + doctor_1);
		System.out.println("Dir. de memòria de doctor_2: " + doctor_2);
		
		Tripulants infermera_1 = contexte.getBean("infermeraNau", Infermera.class);
		Tripulants infermera_2 = contexte.getBean("infermeraNau", Infermera.class);
		
		System.out.println("Patro PROTOTYPE:");
		System.out.println("Dir. de memòria de infermera_1: " + infermera_1);
		System.out.println("Dir. de memòria de infermera_2: " + infermera_2);
		
		System.out.println();
		System.out.println("DESPRÉS DE CARREGAR EL CONTEXT I ABANS DE CARREGAR EL BEAN.");
		ControladorSondes controladorSondes = contexte.getBean("controladorSondesNau", ControladorSondes.class);
		System.out.println("DESPRÉS DE CARREGAR EL BEAN");
		System.out.println(controladorSondes.agafarInformes());
		System.out.println("DESPRÉS D'EXECUTAR controladorSondes.agafarInformes()");
		System.out.println(controladorSondes.agafarTarees());
		System.out.println("DESPRÉS D'EXECUTAR controladorSondes.agafarTarees()");
		System.out.println();
		System.out.println();
		Tripulants especialistaEnECM_10 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		System.out.println("Tripulants especialistaEnECM.agafarInforme(): " + especialistaEnECM_10.agafarInformes());
		System.out.println("Tripulants especialistaEnECM.agafarTarees(): " + especialistaEnECM_10.agafarTarees());
		
		EspecialistaEnECM especialistaEnECM_20 = contexte.getBean("especialistaEnECMTripleInforme", EspecialistaEnECM.class);
		System.out.println("EspecialistaEnECM especialistaEnECM.imprimirTripleInforme(): " + especialistaEnECM_20.imprimirTripleInforme());
		System.out.println("EspecialistaEnECM especialistaEnECM.agafarTarees(): " + especialistaEnECM_20.agafarTarees());
		System.out.println("EspecialistaEnECM especialistaEnECM.getEmail(): " + especialistaEnECM_20.getEmail());
		System.out.println("EspecialistaEnECM especialistaEnECM.getDepartamentNom(): " + especialistaEnECM_20.getDepartamentNom());
		
		contexte.close();
		
		System.out.println("DESPRÉS DE TANCAR EL CONTEXT.");
		
		System.out.println();
		System.out.println("---------- usJavaAnnotationsApartirDeScope - FI ----------");

	}

}
