package curs_de_04_JavaAnnotations;

import org.springframework.stereotype.Component;

@Component
public class InformeDoctor implements InformeInfermeriaInterface {

	@Override
	public String getInformeInfermeria() {
		return "Informe d'infermeria generat pel doctor (generat per Informe.java). ";
	}

}
