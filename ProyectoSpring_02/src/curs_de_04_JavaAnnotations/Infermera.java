package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("infermeraNau")

@Scope("prototype")
public class Infermera implements Tripulants {
	
	private InformeInfermeriaInterface  informeInfermera;
	@Autowired
	@Qualifier("mantenimentInfermeriaMaterial")
	private MantenimentInfermeriaInterface mantenimentMaterial;
	@Autowired
	@Qualifier("mantInferMedica")
	private MantenimentInfermeriaInterface mantenimentMedicaments;
	
	@Override
	public String agafarTarees() {
		return "InfermeraNau: agafarTarees(): ajudar a curar als tripulants. ";
	}

	@Override
	public String agafarInformes() {
		return "infermeraNau: agafarInforme(): Informe de l'infermera de la missió. " + informeInfermera.getInformeInfermeria();
	}
	
	@Autowired
	public void setInformeInfermera(InformeInfermeriaInterface informe) {
		this.informeInfermera = informe;
	}

}
