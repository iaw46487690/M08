package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("doctorNau")
public class Doctor implements Tripulants {
	
	private InformeInfermeriaInterface informeDoctor;
	
	@Autowired
	public Doctor(InformeInfermeriaInterface informeDoctor) {
		this.informeDoctor = informeDoctor;
	}

	@Override
	public String agafarTarees() {
		return "doctorNau: agafarTarees(): curar als tripulants";
	}
	
	@Override
	public String agafarInformes() {
		return "doctorNau: agafarInforme(): Informe del doctor de la missio. "
				+ informeDoctor.getInformeInfermeria();
	}
}
