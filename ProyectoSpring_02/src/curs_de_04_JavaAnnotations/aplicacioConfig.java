package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("curs_de_04_JavaAnnotations")
@PropertySource("classpath:dadesNau.properties")
public class aplicacioConfig {
	
	@Bean
	public InformeElectronicaInterface informeECM() {
		return new InformeECM();
	}
	
	@Bean
	public Tripulants especialistaEnECM() {
		return new EspecialistaEnECM(informeECM());
	}
	
	@Bean
	public InformeElectronicaInterface informeESM() {
		return new InformeESM();
	}
	
	@Bean
	public InformeElectronicaInterface informeEPM() {
		return new InformeEPM();
	}
	
	@Bean
	public Tripulants especialistaEnECMTripleInforme() {
		return new EspecialistaEnECM(informeECM(), informeESM(), informeEPM());
	}
} 
