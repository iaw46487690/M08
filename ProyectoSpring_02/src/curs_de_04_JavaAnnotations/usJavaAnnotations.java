package curs_de_04_JavaAnnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotations {

	public static void main(String[] args) {
		System.out.println("---------- usJavaAnnotations - INICI ----------");
		System.out.println();
		
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		Tripulants doctor = contexte.getBean("doctorNau", Doctor.class);
		
		System.out.println(doctor.agafarTarees());
		System.out.println(doctor.agafarInformes());
		System.out.println("--------------------");
		
		System.out.println();
		
		Tripulants infermera = contexte.getBean("infermeraNau", Infermera.class);
		
		System.out.println(infermera.agafarTarees());
		System.out.println(infermera.agafarInformes());
		System.out.println("--------------------");
		
		contexte.close();
		System.out.println();
		System.out.println("---------- usJavaAnnotations - FI ----------");
	}

}
