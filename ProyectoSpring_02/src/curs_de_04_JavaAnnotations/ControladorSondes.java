package curs_de_04_JavaAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component("controladorSondesNau")
public class ControladorSondes implements Tripulants {
	
	private String nom;
	
	@PostConstruct
	public void inicialitzador() {
		nom = "patata";
		System.out.println("ControladorSondes: S'HA EXECUÇTAT inicialitzador() AMB LA JAVA ANNOTATTION @PostConstruct.");
		System.out.println("Nom: " + nom);
	}
	
	@PreDestroy
	public void finalitzador() {
		nom = "tomaquet";
		System.out.println("ControladorSondes: S'HA EXECUÇTAT finalitzador() AMB LA JAVA ANNOTATTION @PreDestroy.");
		System.out.println("Nom: " + nom);
	}
	
	@Override
	public String agafarTarees() {
		return "controladorSondesNau: agafarTarees()";
	}

	@Override
	public String agafarInformes() {
		return "controladorSondesNau: agafarInforme()";
	}

}
