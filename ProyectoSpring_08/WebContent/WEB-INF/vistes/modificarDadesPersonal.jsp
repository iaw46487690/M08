<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Formulari modificar dades usuari</title>
	</head>
	<body>
		<%@page import="exercici_05_a_08.entitats.Departament"%>
		<%@page import="java.util.List"%>
		<form:form action="processarModificar" modelAttribute="personalTrobat">
			DNI: <form:input path="dni" />
			<form:errors path="nom" style="color:red"></form:errors>
			<br />
			Nom: <form:input path="nom" />
			<br />
			Contrasenya: <form:input path="password" />
			<br />
			Departament:
			<%List<Departament> departs = (List<Departament>)request.getAttribute("departaments"); 
			  for(Departament d:departs) {%>
			  	<input type="radio" name="numDepartament" path="numDepartament" value="<%=d.getID()%>"><%=d.getNom()%>
			  <%}%>
			  <br>
			  <br>
			<input type="submit" value="Submit" />
			<br />
		</form:form>
	</body>
</html>