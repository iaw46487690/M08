<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Login</title>
	</head>
	<body>
		<form:form action="processarLogin" modelAttribute="personalTrobat">
			DNI: <form:input path="dni" />
			<form:errors path="nom" style="color:red"></form:errors>
			<br />
			Contrasenya: <form:input path="password" />
			<br />
			<input type="submit" value="Submit" />
			<br />
			<a href="donarAltaUsuari">Registrar nou usuari</a>
		</form:form>
	</body>
</html>