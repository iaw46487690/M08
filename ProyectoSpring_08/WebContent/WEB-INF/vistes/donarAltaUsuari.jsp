<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Formulari d'alta nou usuari</title>
	</head>
	<body>
		<form:form action="processarAltaUsuari" modelAttribute="nou_usuari">
			DNI: <form:input path="dni" />
			<form:errors path="nom" style="color:red"></form:errors>
			<br />
			Nom: <form:input path="nom" />
			<br />
			Contrasenya: <form:input path="password" />
			<br />
			Departament: <form:input path="numDepartament" />
			<br />
			<input type="submit" value="Submit" />
		</form:form>
	</body>
</html>