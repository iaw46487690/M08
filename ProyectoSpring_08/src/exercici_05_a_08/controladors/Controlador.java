package exercici_05_a_08.controladors;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import exercici_05_a_08.DAOs.*;
import exercici_05_a_08.entitats.Personal;
import exercici_05_a_08.entitats.Departament;

@Controller
public class Controlador {
	@Autowired
	private PersonalDAOInterface personalDAO;
	
	@Autowired
	private DepartamentDAOInterface departamentDAO;
	
	List<Personal> llistaPersonalsDelDAO;
	List<Departament> llistaDepartamentsDelDAO;
	int personalActual = 0;
	
	@RequestMapping("/")
	public String inici() {
		llistaPersonalsDelDAO = personalDAO.getPersonals();
		llistaDepartamentsDelDAO = departamentDAO.getDepartaments();
		System.out.println(llistaPersonalsDelDAO);
		System.out.println(llistaDepartamentsDelDAO);
		return "inici";
	}
	
	@RequestMapping("/login")
	public String mostrarLoginPersonal(Model model) {
		Personal personal = new Personal();
		model.addAttribute("personal", personal);
		return "login";
	}
	
	@RequestMapping("/processarLogin")	
	public String processarLogin(HttpServletRequest request, Model model) {
		boolean trobat = false;
		boolean aux = false;
		int dni;
		String password;
		Personal personal = new Personal();
		model.addAttribute("personal", personal);
		dni = Integer.parseInt(request.getParameter("dni"));
		password = request.getParameter("password");
		System.out.println(dni);
		Personal personalLogetjat = new Personal();
		int index = 0;
		for(Personal personalTmp : llistaPersonalsDelDAO) {
			if (dni == personalTmp.getDni()) {
				trobat = true;
				personalActual = index;
				personal = personalTmp;
				model.addAttribute("existeixLogin", dni);
				System.out.println(personal);
				break;
			}
			index++;
		}
		if(trobat == true) {
			if(password.equalsIgnoreCase(personal.getPassword())) {
				aux = true;
				model.addAttribute("aux", password);
			}
		} else {
			model.addAttribute("error", "ERROR, no existeix aquest DNI.");
			return "login";
		}
		if (aux == true) {
			request.setAttribute("deparaments", llistaDepartamentsDelDAO);
			model.addAttribute("personal", personal);
			return "modificarDadesPersonal";	
		} else {
			model.addAttribute("error", "ERROR, contrasenya incorrecta.");
			return "login";
		}
	}

	@RequestMapping("/processarModificar")
	public String processarModificar(HttpServletRequest request, Model model) {
		int dni = Integer.parseInt(request.getParameter("dni"));
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String nom = request.getParameter("nom");
		String cognom = request.getParameter("cognom");
		int dep = Integer.parseInt(request.getParameter("numDepartament"));
		boolean valid = true;
		int actual = 0;
		for(Personal personalTmp : llistaPersonalsDelDAO) {
			if(actual!=personalActual) {
				if (dni == personalTmp.getDni()) {
					valid = false;
					break;
				}
			}
			actual++;
		}
		if(valid) {
			String nomDep = "";
			int i = 0;
			for(Departament depart : llistaDepartamentsDelDAO) {
				if(personalActual == i) {
					nomDep = depart.getNom();
					break;
				}
				i++;
			}
			Personal personalmod = new Personal();
			personalmod.setDni(dni);
			personalmod.setPass(password);
			personalmod.setNom(nom);
			personalmod.setNumDepartament(dep);
			personalDAO.updatejarPersonal(personalmod);
			llistaPersonalsDelDAO = personalDAO.getPersonals();
			request.setAttribute("departament", nomDep);
			request.setAttribute("personal", llistaPersonalsDelDAO.get(personalActual));
			return "mostrarDadesUsuari";
		}
		else {
			request.setAttribute("departaments", llistaDepartamentsDelDAO);
			model.addAttribute("personal", llistaPersonalsDelDAO.get(personalActual));
			
			return "modificarDadesPersonal";
		}
		
	}
	
	@RequestMapping("/donarAltaUsuari")
	public String mostrarAltaUsuari(Model model) {
		Personal nouPersonal = new Personal();
		model.addAttribute("nou_usuari", nouPersonal);
		return "donarAltaUsuari";
	}
	
	@RequestMapping("/processarAltaUsuari")	
	public String processarAlta(HttpServletRequest request, Model model) {
		boolean totCorrecte = true;
		int dni;
		String password, nom, email, cognom;
		int numDepartament;
		Personal nouPersonal = new Personal();
		model.addAttribute("nou_usuari", nouPersonal);
		dni = Integer.parseInt(request.getParameter("dni"));
		password = request.getParameter("password");
		nom = request.getParameter("nom");
		numDepartament = Integer.parseInt(request.getParameter("numDepartament"));
		email = "prova";
		cognom = "muelle";
		System.out.println(dni);
		for(Personal personalTmp : llistaPersonalsDelDAO) {
			if (dni == personalTmp.getDni()) {
				totCorrecte = false;
				System.out.println("procesarAlta: DNI ja existeix");
			} else if(nom.equalsIgnoreCase(personalTmp.getNom())) {
				totCorrecte = false;
				System.out.println("procesarAlta: NOM ja existeix");
			}
		}
		if(numDepartament != 1 && numDepartament != 2) {
			totCorrecte = false;
			System.out.println("procesarAlta: NUM DEPARTAMENT inexistent");
		}
		if (totCorrecte == true) {
			Personal creatPersonal = new Personal(dni, nom, cognom, email, password, numDepartament);
			personalDAO.insertarPersonal(creatPersonal);
			llistaPersonalsDelDAO = personalDAO.getPersonals();
			int i = 1;
			for(Personal persTmp : llistaPersonalsDelDAO) {
				System.out.println(i + ": Personal" + 
						"\n      pers.getId() = " + persTmp.getDni() +
						"\n      pers.getNom() = " + persTmp.getNom() +
						"\n      pers.getDep() = " + persTmp.getNumDepartament() +
						"\n      pers.getPassword() = " + persTmp.getPassword());
				i++;
			}
			Personal personal = new Personal();
			model.addAttribute("personal", personal);
			return "login";	
		} else {
			System.out.println("procesarAlta: MALAMENT");
			model.addAttribute("error", "ERROR, Alta incorrecta.");
			return "donarAltaUsuari";
		}
	}
}