package exercici_05_a_08.DAOs;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import exercici_05_a_08.entitats.Personal;

@Repository
public class PersonalDAOClasse implements PersonalDAOInterface{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	@Override
	public List<Personal> getPersonals() {
		
		Session session = sessionFactory.getCurrentSession();
		Query<Personal> llistaPersonals = session.createQuery("from Personal", Personal.class);
		
		List<Personal> llistaPersonalDeLaBD = llistaPersonals.getResultList();
		
		return llistaPersonalDeLaBD;
	}
	
	@Override
	@Transactional
	public void insertarPersonal(Personal personalNou) {
		Session session = sessionFactory.getCurrentSession();
		session.save(personalNou);
	}
	
	@Override
	@Transactional
	public Personal getPersonal(int personalDNI) {
		Session session = sessionFactory.getCurrentSession(); 
		Personal personalTmp = session.get(Personal.class, personalDNI);
		return personalTmp;
	}
	
	@Override
	@Transactional
	public void updatejarPersonal(Personal personalPerUpdatejar) {
		Session session = sessionFactory.getCurrentSession();
		session.update(personalPerUpdatejar);
	}
	
	@Override
	@Transactional
	public void eliminarPersonal(int personalDNI) {
		Session session = sessionFactory.getCurrentSession();
		Query EsborrarPersonal = session.createQuery("DELETE from Personal where dni = " + personalDNI);
		EsborrarPersonal.executeUpdate();
	}
}
