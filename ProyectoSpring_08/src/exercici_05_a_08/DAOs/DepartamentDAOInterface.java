package exercici_05_a_08.DAOs;
import java.util.List;
import exercici_05_a_08.entitats.Departament;

public interface DepartamentDAOInterface {
	public List<Departament> getDepartaments();
	public Departament getDepartament(int departamentId);
	public void updatejarDepartament(Departament departamentPerUpdatejar);
	public void eliminarDepartament(int departamentId);
	void insertarDepartament(Departament departamentNou);

}
