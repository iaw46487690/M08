package exercici_05_a_08.DAOs;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import exercici_05_a_08.entitats.Departament;

@Repository
public class DepartamentDAOClasse implements DepartamentDAOInterface {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	@Override
	public List<Departament> getDepartaments() {
		
		Session session = sessionFactory.getCurrentSession();
		Query<Departament> llistaDepartaments = session.createQuery("from Departament", Departament.class);
		
		List<Departament> llistaDepartamentDeLaBD = llistaDepartaments.getResultList();
		
		return llistaDepartamentDeLaBD;
	}
	
	@Override
	@Transactional
	public void insertarDepartament(Departament departamentNou) {
		Session session = sessionFactory.getCurrentSession();
		session.save(departamentNou);
	}
	
	@Override
	@Transactional
	public Departament getDepartament(int departamentId) {
		Session session = sessionFactory.getCurrentSession(); 
		Departament departamentTmp = session.get(Departament.class, departamentId);
		return departamentTmp;
	}
	
	@Override
	@Transactional
	public void updatejarDepartament(Departament departamentPerUpdatejar) {
		Session session = sessionFactory.getCurrentSession();
		session.update(departamentPerUpdatejar);
	}
	
	@Override
	@Transactional
	public void eliminarDepartament(int departamentId) {
		Session session = sessionFactory.getCurrentSession();
		Query esborrarDepartament = session.createQuery("DELETE from Departament where id = " + departamentId);
		esborrarDepartament.executeUpdate();
	}
}
