package exercici_05_a_08.DAOs;
import java.util.List;
import exercici_05_a_08.entitats.Personal;

public interface PersonalDAOInterface {
	public List<Personal> getPersonals();
	public Personal getPersonal(int personalDni);
	public void updatejarPersonal(Personal personalPerUpdatejar);
	public void eliminarPersonal(int personalDni);
	void insertarPersonal(Personal personalNou);

}
