package exercici_05_a_08.entitats;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="departament")
public class Departament {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="nom")
	private String nom;
	@Column(name="email")
	private String email;
	@Column(name="capDepartament")
	private int capDepartament;
	@Column(name="llistaPersonal")
	private String llistaPersonal;
	
	public Departament() {
		
	}
	
	public Departament(int id) {
		this.id = id;
	}
	
	public Departament(int id, String nom, String email) {
		this.id = id;
		this.nom = nom;
		this.email = email;
	}
	
	public int getID() {
		return id;
	}
	public void setID(int iD) {
		id = iD;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getCapDepartament() {
		return capDepartament;
	}
	public void setCapDepartament(int capDepartament) {
		this.capDepartament = capDepartament;
	}
	public String getLlistaPersonal() {
		return llistaPersonal;
	}
	
	public void setLlistaPersonal(String llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Departament other = (Departament) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Departament Id = " + id + ", Nom = " + nom + ", Email = " + email + ", Cap de departament = " + capDepartament
				+ ", Llista Personal = " + llistaPersonal;
	}
	

}
