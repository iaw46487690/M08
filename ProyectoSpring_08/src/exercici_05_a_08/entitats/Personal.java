package exercici_05_a_08.entitats;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Table(name="personal")
public class Personal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="dni")
	private int dni;
	
	@Column(name="nom")
	private String nom;
	@Column(name="cognom")
	private String cognom;
	@Column(name="email")
	private String email;
		
	@NotNull
	@Size(min = 7, message="La contrasenya te un minim de 7 caracters")
	@Column(name="password")
	private String password;
	@Column(name="numDepartament")
	private int numDepartament;
	
	public Personal() {
	}
	
	
	
	public Personal(int dni) {
		super();
		this.dni = dni;
	}

	public Personal(int dni, String nom,
			String cognom,
			String email,
			String password,
			int numDepartament) {
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.email = email;
		this.password = password;
		this.numDepartament = numDepartament;
	}
	
	public Personal(int dni, String nom, String password, int numDepartament) {
		this.dni = dni;
		this.nom = nom;
		this.password = password;
		this.numDepartament = numDepartament;
	}




	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getNumDepartament() {
		return numDepartament;
	}
	public void setNumDepartament(int numDepartament) {
		this.numDepartament = numDepartament;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPass(String password) {
		this.password = password;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cognom == null) ? 0 : cognom.hashCode());
		result = prime * result + dni;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + numDepartament;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personal other = (Personal) obj;
		if (cognom == null) {
			if (other.cognom != null)
				return false;
		} else if (!cognom.equals(other.cognom))
			return false;
		if (dni != other.dni)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (numDepartament != other.numDepartament)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Dni = " + dni + ", Nom = " + nom + ", Cognom = " + cognom + ", Email = " + email + ", Contrasenya = " + password
				+ ", numDepartament=" + numDepartament;
	}
	
	
	
	
}