package curs_de_08_Acceso_a_datos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class HQLUpdatePlanetes {

	public static void main(String[] args) {
		// System.out.println("Creem l'objecte SessionFactory.");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció.");
			elMeuSession.beginTransaction();
			
			System.out.println();
			
			// RECORDAR!!!: les sentències són contra les classes i no contra les taules de la BD.
			
			// 1a forma de fer un UPDATE:
			// Agafem de la BD el planeta amb Id = 35.
			System.out.println("Fem un UPDATE amb la 1a forma:");			
			int planetaId = 35;
			Planetes planetaTmp = elMeuSession.get(Planetes.class, planetaId);
			
			// Modificarem el valor d'un atribut a través del setter pertinent.
			planetaTmp.setNom("Venus");
			
			System.out.println();
			
			// 2a forma de fer un UPDATE (fent servir el llenguatge HQL):
			System.out.println("Fem un UPDATE amb la 2a forma (fent servir el llenguatge HQL):");
			String sentenciaHQL = "UPDATE Planetes SET nom = 'Jupiter' WHERE descripcio LIKE '%ROIG%'";
			System.out.println("1: LLançem la sentència '" + sentenciaHQL + "'.");
			elMeuSession.createQuery(sentenciaHQL).executeUpdate();
			
			elMeuSession.getTransaction().commit();		// Perquè s'executi la transacció. 
														// SI ES FAN CONSULTES SEMBLA QUE NO ES NECESSARI.
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}
}
