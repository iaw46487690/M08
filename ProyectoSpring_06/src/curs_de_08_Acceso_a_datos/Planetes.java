package curs_de_08_Acceso_a_datos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


// @Entity: les classes que serviran per a manipular la BD s'identifiquen amb aquesta Java Annotation. Aquestes 
// classes també són conegudes com POJO's (Plain Old Java Objects) i qualsevol classe ho pot ser.

// @Table: per a especificar amb quina taula està relacionada la classe.

@Entity
@Table(name="planetes")
public class Planetes {
	// @Id: per indicar que l'atribut de la classe correspon amb una columna de la taula que és clau primaria.
	// @GeneratedValue: per a generar un valor automàticament. Amb IDENITY està fent un AUTO_INCREMENT numèric.
	// @Column: per a determinar quina columna de la taula s'ha de connectar amb l'atribut.
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="descripcio")
	private String descripcio;
	
	@Column(name="dataDescubriment")
	private LocalDate dataDescubriment;

	
	public Planetes() {
	}

	public Planetes(String nom, String descripcio, LocalDate dataDescubriment) {
		this.nom = nom;
		this.descripcio = descripcio;
		this.dataDescubriment = dataDescubriment;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public LocalDate getDataDescubriment() {
		return dataDescubriment;
	}

	public void setDataDescubriment(LocalDate dataDescubriment) {
		this.dataDescubriment = dataDescubriment;
	}

	@Override
	public String toString() {
		return "Planetes [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", dataDescubriment="
				+ dataDescubriment + "]";
	}
	
}
