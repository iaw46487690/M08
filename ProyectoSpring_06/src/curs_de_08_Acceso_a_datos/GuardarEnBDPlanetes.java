package curs_de_08_Acceso_a_datos;

import java.time.LocalDate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GuardarEnBDPlanetes {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory.");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();

		System.out.println("Creem 2 objectes de tipus Planetes.");
		Planetes planeta_1 = new Planetes("Mart", "Planeta roig", LocalDate.of(1610, 12, 31));
		Planetes planeta_2 = new Planetes("Saturn", "Planeta gaseós", LocalDate.of(1610, 1, 18));
		
		try {
			// Insertem 2 registres en la BD.
			System.out.println("Iniciem la transacció.");
			
			elMeuSession.beginTransaction();
			
			elMeuSession.save(planeta_1);
			
			// flush() i clear() són necessaris per a poder fer un 2n save() i així poder fer 2 INSERT's en la BD.
			elMeuSession.flush();
			
			elMeuSession.clear();
			
			elMeuSession.save(planeta_2);
			
			elMeuSession.getTransaction().commit();		// Perquè s'executi la transacció.
			
			System.out.println("Acabem la transacció de Mart. Registre INSERTAT en la BD.");
			
			System.out.println("Acabem la transacció de Saturn. Registre INSERTAT en la BD.");
			
			
			// LLegirem 1 registre de la BD.
			// PROBLEMA: quan vam fer els 2 objectes planetes no els vam donar cap valor a l'atribut id perquè
			// volíem que el valor li donés el MySQL ja que el camp és autonumèric i és MySQL qui, fins ara,
			// porta el control d'aquest camp (quin número s'ha de donar en tot moment).
			//
			// Com que vam fer un commit(), la transacció es va acabar i ara hem de fer una nova.
			elMeuSession.beginTransaction();
			
			System.out.println("Lectura del registre amb id = " + planeta_1.getId());
			
			Planetes planetaLLegitDeLaBD = elMeuSession.get(Planetes.class, planeta_1.getId());
			
			System.out.println("Registre llegit de la BD: " + planetaLLegitDeLaBD);
			
			elMeuSession.getTransaction().commit();		// Perquè s'executi la nova transacció.
			
			System.out.println("Hem acabat de llegir un registre de la BD.");
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}

	}

}
