package curs_de_08_Acceso_a_datos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class HQLDeletePlanetes {

	public static void main(String[] args) {
		// System.out.println("Creem l'objecte SessionFactory.");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció.");
			elMeuSession.beginTransaction();
			
			System.out.println();
			
			// RECORDAR!!!: les sentències són contra les classes i no contra les taules de la BD.
			
			String sentenciaHQL = "DELETE Planetes WHERE nom = 'Jupiter'";
			System.out.println("1: LLançem la sentència '" + sentenciaHQL + "'.");
			elMeuSession.createQuery(sentenciaHQL).executeUpdate();
			
			elMeuSession.getTransaction().commit();		// Perquè s'executi la transacció. 
														// SI ES FAN CONSULTES SEMBLA QUE NO ES NECESSARI.
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}
}
