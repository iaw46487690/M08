package curs_de_08_Acceso_a_datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectorBD {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Hem d'especificar la URL (la direcció) on es trova el nostre servidor MySQL (el de la BD).
		// El format per a connectar-se a una BD MySQL és: "jdbc:mysql://" + la IP del servidor + ":" + el port de sortida
		// del server MySQL (per defecte és el 3306) + "/" + nom de la nostra BD.
		// Li afegim "useSSL=false" per a indicar que ens connectarem amb la BD sense seguretat SSL.
		String servidorBDUrl = "jdbc:mysql://localhost:3306/LeonovBD?useSSL=false&serverTimezone=UTC"; 
		
		// Si hi hagués cap error referent a la "time zone", hem d'afegir &serverTimezone=UTC al final de servidorBDUrl
		// perquè al final ens quedi: 
		// String servidorBDUrl = "jdbc:mysql://localhost:3306/LeonovBD?useSSL=false&serverTimezone=UTC";
		
		// Indiquem quin és l'usuari i el password que farem servir per a connectar-nos a la BD del MySQL.
		
		String usuari = "root";
		
		String contrasenya = "88pak75gpr";
		
		
		System.out.println("INICIANT CONNEXIO AMB LA BD: " + servidorBDUrl);
		
		try {
			// Per a connectar-nos a una BD farem servir la classe Connection.
			// Farem servir el mètode getConnection que té 3 paràmetres.
			// Fem una importació del paquet java.sql
			Connection conexio = DriverManager.getConnection(servidorBDUrl, usuari, contrasenya);
						
			System.out.println("CONNEXIO AMB EXIT");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("e.getMessage() = " + e.getMessage());
			e.printStackTrace();
		}
		
		
		
	}

}
