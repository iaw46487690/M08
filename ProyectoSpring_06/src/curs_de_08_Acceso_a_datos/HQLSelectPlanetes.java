package curs_de_08_Acceso_a_datos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class HQLSelectPlanetes {

	public static void main(String[] args) {
		// System.out.println("Creem l'objecte SessionFactory.");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció.");
			elMeuSession.beginTransaction();
			
			System.out.println();
			
			// RECORDAR!!!: les consultes són contra les classes i no contra les taules de la BD.
			
			// Consulta dels planetes sense condicions.
			// Com que al fer una consulta no sabrem si ens retornarà 1 o més resultats (1 o més
			// registres de la BD), hem de ficar el resultat de la consulta en una llista.
			String sentenciaHQL = "FROM Planetes";
			System.out.println("1: LLançem la consulta '" + sentenciaHQL + "'.");
			List<Planetes> llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for (Planetes planetaTmp : llistaPlanetes) {
				System.out.println(planetaTmp);
			}
			
			System.out.println();
			
			// Consulta dels planetes amb condicions.
			sentenciaHQL = "FROM Planetes WHERE nom = 'Mart'";
			System.out.println("2: LLançem la consulta '" + sentenciaHQL + "'.");
			llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for (Planetes planetaTmp : llistaPlanetes) {
				System.out.println(planetaTmp);
			}
			
			System.out.println();
			
			// Consulta dels planetes amb condicions i fent servir un alias de la classe.
			sentenciaHQL = "FROM Planetes p WHERE p.nom = 'Saturn'";
			System.out.println("3: LLançem la consulta '" + sentenciaHQL + "'.");
			llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for (Planetes planetaTmp : llistaPlanetes) {
				System.out.println(planetaTmp);
			}
			
			System.out.println();
			
			// Consulta dels planetes amb condicions + operador lògic.
			sentenciaHQL = "FROM Planetes p WHERE p.nom = 'Mart' OR p.descripcio LIKE '%gaseós%'";
			System.out.println("4: LLançem la consulta '" + sentenciaHQL + "'.");
			llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for (Planetes planetaTmp : llistaPlanetes) {
				System.out.println(planetaTmp);
			}
			
			
			elMeuSession.getTransaction().commit();		// Perquè s'executi la transacció. 
														// SI ES FAN CONSULTES SEMBLA QUE NO ES NECESSARI.
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}
}
