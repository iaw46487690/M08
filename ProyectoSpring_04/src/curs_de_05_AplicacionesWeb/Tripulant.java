package curs_de_05_AplicacionesWeb;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import validacionsPersonalitzades.telefonFinlandia;

public class Tripulant {
	@NotNull
	@Size(min = 1, message = "Long, mínima de nom ha de ser 1 caràcter.")
	private String nom;
	private String cognom;
	private String departament;
	private String coneixements;
	private String ciutatNaixement;
	private String idiomes;
	@NotNull
	@Min(value = 18, message = "El nou tripulant ha de tenir com a mínim 18 anys")
	@Max(value = 65, message = "El nou tripulant ha de tenir com a màxim 65 anys")
	private int edat;
	@NotBlank(message = "S'ha d'omplir l'email.")
	@Email
	@Size(min = 5, message = "Email no vàlid.")
	private String email;
	@Pattern(regexp="[0-9]{5}", message = "Només 5 númeos")
	private String codiPostal;
	@telefonFinlandia
	private String telefon;
	
	public String getCodiPostal() {
		return codiPostal;
	}
	public void setCodiPostal(String codiPostal) {
		this.codiPostal = codiPostal;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getConeixements() {
		return coneixements;
	}
	public void setConeixements(String coneixements) {
		this.coneixements = coneixements;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getDepartament() {
		return departament;
	}
	public void setDepartament(String departament) {
		this.departament = departament;
	}
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}
	public String getIdiomes() {
		return idiomes;
	}
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
