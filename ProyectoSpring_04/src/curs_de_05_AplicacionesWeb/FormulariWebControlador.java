package curs_de_05_AplicacionesWeb;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class FormulariWebControlador {
	// Com es fa un Controlador:
	// 1r: hem de crear 1 mètode per a proporcionar a l'usuari el formulari que volem que ompli.
	// 2n: hem de crear 1 mètode que rebi el formulari omplert per l'usuari i el procesi i l'invii una resposta.
	
	// El 1r pas:
	@RequestMapping("/mostrarFormulari")		// Aquesta javaAnnotation determina quina URL ha de fer servir l'usuari per a demanar el formulari. La "/" és obligatoria. Generalment es posa el nom del mètode.
	public String mostrarFormulari() {			// L'usuari per a demanar la vista "formulariNom" ha de cridar a la URL "mostrarFormulari (vistaExemple_1.jsp té un <a href="mostrarFormulari">)".
		return "formulariNom";					// El formulari que demana l'usuari per omplir és la vista "formulariNom.jsp".
	}
	
	// El 2n pas:
	@RequestMapping("/respostaDelFormulari_28")	// Aquesta java Annotation determina quina URL (pàgina web) rebrà el formulari omplert, el procesarà i enviarà una resposta.
	public String respostaDelFormulari() {		// La vista formulariNom.jsp té un <form action="respostaDelFormulari_28"> i a llavors crida a aquesta funció que retornarà la vista "formulariNomResposta".
		return "formulariNomResposta";			// Aquesta serà la resposta que enviarà el sistema al rebre el formulari (la vista que farà servir).
	}
	
	
	// Afegir dades al model (video 29):
	// Volem agafar les dades del formulari que ha omplert l'usuari i volem modificar-les/afegir coses (afegirem/modificarem dades del model).
	@RequestMapping("/respostaDelFormulari_29")
	public String procesarFormulari(HttpServletRequest request, Model model) {		// L'objecte "request" ens servirà per accedir a les dades transmeses pel formulari.
																					// El "model" és l'objecte que serveix per a transmetre informació entre el controlador i les vistes.
		String nomTripulant = request.getParameter("tripulantNom_29");

		nomTripulant = nomTripulant + " és un tripulant de la CCCP Leonov";
		
		String resposta = "Qui hi ha (video 29)? " + nomTripulant;
		
		// Agregem la informació al model:
		model.addAttribute("missatgePerRetornar_29", resposta);		// Spring associa el contingut de "resposta" a "missatgePerRetornar" (per accedir
																	// al contingut de "resposta", Spring fa servir "missatgePerRetornar_29").
		
		return "formulariNomResposta";								// Aquesta serà la resposta que enviarà el sistema al rebre el formulari (la vista que farà servir).
	}
	
	
	// Video 31: arxius WAR 
	// Comprimir (empaquetar) el nostre projecte web en un arxiu .war de manera que si posem l'arxiu en un servidor web, executarà el nostre projecte.
	// En l'arxiu WAR es ficaran tots els arxius .java, les llibreries (carpeta Libraries), WebContent/*.*, ...
	// Tots els servidors JEE (ex: Tomcat) són capaços de llegir els WAR però hem de ficar l'arxiu WAR en la carpeta adecuada del server (del Tomcat en el nostre cas).
	// btn dret sobre el projecte --> Export --> WAR file --> en la casella Destination posem a on volem que ens copii el WAR. Després haurem de copiar el WAR en la carpeta adecuada del server.
	//
	// El server (Tomcat) l'hauriem d'instalar en el SO. ALERTA: EN EL VIDEO 4 QUAN VAM INSTALAR ECLIPSE, JEE, JDK,.. TAMBÉ VAM INSTALAR EL TOMCAT (QUE ES VA INSTALAR EN EL UBUNTU).
	//
	// Dins del directori on s'instala Tomcat existeix el directori "webapps" que és on copiarem l'arxiu WAR.
	// Després de copiar l'arxiu WAR, Tomcat crearà un directori amb el mateix nom on descomprimirà el nostre projecte (triga uns segons a fer-ho).
	// Executem un navegador web y posem "localhost:8080/ProyectoSpringTres". El "8080" si Tomcat surt per aquest port i sino el que usi. El "ProyectoSpringTres" és el nom del WAR sense extensió (en realitat
	// estem accedint al directori que ha creat Tomcat a partir del nostre WAR).
	
	
	// Afegir dades al model (video 32) amb @RequestParam:
	// Volem agafar les dades del formulari que ha omplert l'usuari i volem modificar-les/afegir coses (afegirem/modificarem dades del model).
	@RequestMapping("/respostaDelFormulari_32")
	public String procesarFormulari(@RequestParam("tripulantNom_32") String nomTripulant, Model model) {		// La java Annotation @RequestParam serveix per a fer lo mateix que lo del video 29.

		nomTripulant = nomTripulant + " és un tripulant de la CCCP Leonov";
		
		String resposta = "Qui hi ha (video 32)? " + nomTripulant;
		
		// Agregem la informació al model:
		model.addAttribute("missatgePerRetornar_32", resposta);		// Spring associa el contingut de "resposta" a "missatgePerRetornar" (per accedir
																	// al contingut de "resposta", Spring fa servir "missatgePerRetornar_32").
		
		return "formulariNomResposta";								// Aquesta serà la resposta que enviarà el sistema al rebre el formulari (la vista que farà servir).
	}
}
