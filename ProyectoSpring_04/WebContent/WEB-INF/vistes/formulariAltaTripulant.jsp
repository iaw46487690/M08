<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulari d'alta d'un tripulant - CCCp LEONOV</title>
</head>
<body>
	<form:form action="procesarAltaTripulant" modelAttribute="el_doctor">
		Nom del tripulant: <form:input path="nom"/>
		<form:errors path="nom" style="color:red"></form:errors>
		<br/>
		<br/>
		Cognom del tripulant: <form:input path="cognom"/>
		<br/>
		<br/>
		Edat del tripulant: <form:input path="edat"/>
		<form:errors path="edat" style="color:red"></form:errors>
		<br/>
		<br/>
		Email del tripulant: <form:input path="email"/>
		<form:errors path="email" style="color:red"></form:errors>
		<br/>
		<br/>
		Codi postal: <form:input path="codiPostal"/>
		<form:errors path="codiPostal" style="color:red"></form:errors>
		<br/>
		<br/>
		Telèfon: <form:input path="telefon"/>
		<form:errors path="telefon" style="color:red"></form:errors>
		<br/>
		<br/>
		<input type="submit" value="Donar d'alta"/>
		
	</form:form>
	<br/>
	<br/>
	Departament (només 1): <form:select path="departament">
		<form:option value="infermeria">Infermeria</form:option>
		<form:option value="maquines">Màquines</form:option>
		<form:option value="pont" label="Pont"/>
	</form:select>
	<br/>
	<br/>
	Conneixements (seŀlecció múltiple): <form:select path="conneixements" multiple="true">
		<form:option value="biologia" label="Biologia"/>
		<form:option value="quimica" label="Quimica"/>
		<form:option value="antropologia" label="Antropologia"/>
		<form:option value="biologiaMolecular" label="Biologia molecular"/>
	</form:select>
	<br/>
	Seleccionar una ciutat d'origen:
	<form:radiobutton path="ciutatNaixement" value="Barcelona" label="Barcelona"/>
	<form:radiobutton path="ciutatNaixement" value="Estocolm" label="Estocolm"/>
	<form:radiobutton path="ciutatNaixement" value="Helsinki" label="Helsinki"/>
	<form:radiobutton path="ciutatNaixement" value="Oslo" label="Oslo"/>
	<br/>
	<br/>
	<form:checkbox path="idiomes" value="castella" label="Castellà"/>
	<form:checkbox path="idiomes" value="catala" label="Català"/>
	<form:checkbox path="idiomes" value="finlandes" label="Finlandès"/>
	<form:checkbox path="idiomes" value="suec" label="Suec"/>
</body>
</html>