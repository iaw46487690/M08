<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>vistaExemple_1.jsp(Pagina inicial)</title>
</head>
<body>
	<!-- Aquí hem de ficar el nostre codi (el que construirà la pàgina web que farà d'interficie amb l'usuari) .-->
	
	<center>
		<h1>1r exemple de MVC amb Spring</h1>
		(hem cridat a vistaExemple_1.jsp)<br>
		(Aquesta és la pàgina inicial perquè hi ha 1 classe de tipo  @Controller que té una funció amb
		un RequestMapping sense paràmetre)
	</center>
	<br>
	<br>
	<!-- Ara creem una crida al formulari que volem que ompli l'usuari (video 28) -->
	<!-- Com a direcció del formulari hem de posar el valor del paràmetre del @RequestMapping("/mostrarFormulari") que tenim en FormulariWebControlador.java -->
	<a href="mostrarFormulari">Anar al formulari</a> 
	<br>
	<br>
	<a href="tripulant/mostrarFormulariAltaTripulant">Anar al formulari d'alta d'un tripulant</a>
</body>
</html>
