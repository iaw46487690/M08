<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulari logeig</title>
</head>
<body>
	<form:form action="" modelAttribute="">
		DNI: <form:input path="dni"/>
		<form:errors path="dni" style="color:red"></form:errors>
		<br/>
		<br/>
		Contraseña: <form:input path="contraseña"/>
		<form:errors path="contraseña" style="color:red"></form:errors>
		<br/>
		<br/>
		<input type="submit" value="login"/>
	</form:form>
</body>
</html>