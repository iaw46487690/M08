package practica05_07;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class Controlador {
	@RequestMapping("/")
	public String mostrarInici() {
		return "inici";
	}
}
