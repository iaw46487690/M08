<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>formulariNomResposta</title>
</head>
<body>
	Video 28 (omplert el formulari del video 28): <br>
	Aquesta és la resposta que envia el server al formulari que ha enviar l'usuari.<br>
	El nom del tripulant enviat per l'usuari a través del formulari ha estat: <b> ${param.tripulantNom_28} </b>
	<br>
	<br>
	Video 29 (omplert el formulari del video 29): <br>
	<b>missatgePerRetornar:</b> ${missatgePerRetornar_29}
</body>
</html>